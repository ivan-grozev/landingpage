var laptops = [
    {
        "name": "Dell Inspiron 3582 ",
        "id": 131799,
        "part_num": 1,
        "colors": [
            {
                color: 'black',
                text: 'Черен',
                id: 131799,
                img: "https://www.pic.bg/image/show?f=1562938847.jpg&w=580&h=430"
            }, 
            {
                color: 'silver',
                text: 'Сребрист',
                id: 131810,
                img: "https://www.pic.bg/ckeditor_assets/pictures/2658/content_dell-inspiron-5584-silver-landing-page_jpg.jpg"
            }             
        ],
        "url": "https://www.pic.bg/laptop-dell-inspiron-3582-5397184273432-131799",
        "img": "https://www.pic.bg/image/show?f=1562938282.jpg&w=580&h=430",
        "cpu": "Intel® Core™ i5-9300H 2.40 GHz up to 4.10 GHz , 8MB cache",
        "ram": "8GB 2666MHz (1x8GB) Тип: DDR4",
        "display": "15.6\" (39.62 cm) FullHD IPS Anti-Glare 1920x1080 матов",
        "vga": "NVIDIA® GeForce® GTX 1650 (4GB GDDR5)",
        "hdd": "Без HDD",
        "ssd": "256GB PCIe NVMe SSD",
        "variations": [
            {
                ssd: '256',
                price: 539,
                label: "Базов",
                label_ram: "4GB 2666Mhz Тип: DDR4",
                label_ssd: "256GB PCIe NVMe SSD",
                link: "/131799",
                colors: [
                    { color: "black",
                      id: 131799
                    },
                    { color: "silver",
                      id: 131810
                    }
                ]
            },
            {
                ssd: 500,
                price: 1709,
                label: "500GB SSD",
                label_ram: "4GB 2666Mhz Тип: DDR4",
                label_ssd: "500GB PCIe NVMe SSD",
                link: "/137526",
                colors: [
                    { color: "black",
                      id: 137526
                    },
                    { color: "silver",
                      id: 131817
                    }
                ]
            },
            {
                ssd: 501,
                price: 819,
                label: "8GB RAM и 500GB SSD",
                label_ram: "8GB 2666Mhz Тип: DDR4",
                label_ssd: "500GB PCIe NVMe SSD",
                link: "/137863",
                colors: [
                    { color: "black",
                    id: 137863
                  },
                  { color: "silver",
                    id: 137862
                  }
                ]
            },
            {
                ssd: 502,
                price: 739,
                label: "16GB RAM и 1TB SSD",
                label_ssd: '1TB PCIe NVMe',
                label_ram: '16GB 2666MHz DDR4',
                link: "/137526",
                colors: [              
                    { color: "black",
                      id: 137526
                    },
                    { color: "silver",
                      id: 137525
                    }

                ]
            }
        ]
    },

    {
        "name": "Lenovo Legion Y540-15IRH-PG0",
        "id": 131800,
        "part_num": 2,
        "colors": [
            
        ],
        "url": "https://www.pic.bg/laptop-lenovo-legion-y540-15irh-pg0-81sy00e7bm-139474",
        "img": "/ckeditor_assets/pictures/2769/content_03_legion_y540_product_photography_15inch_hero_front_facing_left.png",
        "cpu": "Intel® Core™ i7-9750H 2.60 GHz - 4.50 GHz, 12MB Cache (6-ядрен)",
        "ram": "8GB 2666MHz Тип: DDR4",
        "display": "15.6\" (39.62 cm) FullHD IPS 250nits 60Hz 1920x1080 матов",
        "vga": "NVIDIA GeForce GTX 1650 (4GB GDDR5)",
        "hdd": "Без HDD",
        "ssd": "512GB SSD PCIe NVMe",
        "variations": [
            {
                ssd: 500,
                price: 1949,
                label: "Базов",
                link: "/139474",
                colors: [
                    {
                    }
                ]
            },
            {
                ssd: 1000,
                price: 2119,
                label: "1TB SSD",
                link: "/139476",
                colors: [
                    {
                    }
                ]
            },
            {
                ssd: 501,
                price: 2049,
                label: "16GB RAM",
                link: "/139475",
                colors: [
                    {
                    }
                ]
            },
            {
                ssd: 1001,
                price: 2219,
                label: "16GB RAM и 1TB SSD",
                link: "/139477",
                colors: [
                    {
                    }
                ]
            }
        ]

    }
];
var products = [];
var color = {};
var value = {};
var type = 'ssd';
for (var i = 0; i < laptops.length; i++) {
    var laptop = laptops[i];
    //console.log(laptop);
    var html = '<div class="row product-cust-containers">';
    html += "<div class=\"col-xs-12 col-md-7 col-lg-7\">";
    html += "<h3 class='color-heading text-center'>" + laptop.name + "</h3>";
    html += "<img src='" + laptop.img + "' class=\"img-responsive big-img-" + laptop.id + "\">";

    if (laptop.colors.length > 1) {
        html += '<div class="row thumbnail-container text-center">';
        html += '<div class="col-xs-12 text-center choose-color-heading color-heading-tumb">Избери цвят</div><br /><br />';
        for (var lc = 0; lc < laptop.colors.length; lc++) {
            var laptop_color = laptop.colors[lc];
            color[laptop_color.id] = laptop_color.color;
            value[laptop_color.id] = 0;
            html += "<div class='col-xs-3 col-centered'>";
            html += '<img class="color img-responisve img-thumbnail img-thumb-color center-block" data-id="' + laptop.id + '" data-color="' + laptop_color.color + '" data-colorid="' + laptop_color.id + '" src="' + laptop_color.img + '" width="100">';
            html += '<p class="text-center color-heading-tumb">' + laptop_color.text + '</p>';
            html += "</div>";
        }
        html += '</div>';
    }
    html += '</div>';
    html += "<div class=\"col-xs-12 col-sm-12 col-md-5 col-lg-5 aspire5-description-container\">";
    html += "<ul class='text-left list list-unstyled landing-description'>";
    html += "<li class='cpu-info'><span>Процесор</span><span class='glyphicon glyphicon-info-sign mobile-info trans-info hidden-lg hidden-md' data-placement='top' data-toggle='tooltip' title='" + laptop.cpu + "' data-original-title='test'></span><br /> <p class='info-mobile-align'>" + laptop.cpu + "</p></li>"; 
    html += "<li><span>Екран</span><span class='glyphicon glyphicon-info-sign mobile-info trans-info hidden-lg hidden-md' data-placement='top' data-toggle='tooltip' title='" + laptop.display + "' data-original-title=''></span><br /> <p class='info-mobile-align'>" + laptop.display + "</p> </li>";
    html += "<li><span>Памет</span><br /> <p class='info-mobile-align description-ram-" + laptop.id + "'>" + laptop.ram + "</p></li>";
    html += "<li><span>Видеокарта</span><span class='glyphicon glyphicon-info-sign mobile-info trans-info xs-view-only' data-placement='top' data-toggle='tooltip' title='" + laptop.vga + "' data-original-title=''></span><br /> <p class='info-mobile-align'>" + laptop.vga + "</p></li>";
    html += "<li><span>SSD</span><div> <p class='info-mobile-align description-ssd-" + laptop.id + "'>" + laptop.ssd + "</p></div></li>";

    html += "<li><span>Хардк диск</span><br /> <p class='info-mobile-align'>" + laptop.hdd + "</p></li>";
    html += "</ul>";
    html += "<div class='choose-upgrade'>";
    html += '<i aria-hidden="true" class="fa fa-hdd-o hdd-acer">&nbsp;</i> <span class="choose-model-aspire5">Избери UPGRADE</span>';
    html += '</div>';
    html += "<div class='button-section'>";
    html += '<br /><ul class="nav nav-tabs text-left text-sm-center" role="tablist">';

    var variations = laptop.variations;

    for (var v = 0; v < variations.length; v++) {
        var ssd_variation = variations[v];
        
        var ram = typeof (ssd_variation.label_ram) != 'undefined' ? ssd_variation.label_ram : '';
        var hdd = typeof (ssd_variation.label_hdd) != 'undefined' ? ssd_variation.label_hdd : '';
        var ssd = typeof (ssd_variation.label_ssd) != 'undefined' ? ssd_variation.label_ssd : ssd_variation.label;
        
        html += '<li class="' + (v == 0 ? 'active' : '') + '" role="presentation"><a aria-controls="home" data-id="' + laptop.id + '" data-hdd="' + hdd + '" data-ram="' + ram + '" data-ssd="' + ssd + '" data-value="' + ssd_variation.ssd + '" data-toggle="tab" href="#p' + laptop.id + '-v' + ssd_variation.ssd + '" role="tab">' + ssd_variation.label + '</a></li>';
        
        //if(ssd_variation.colors.length>1) {
            for (var c = 0; c < ssd_variation.colors.length; c++) {
                var ssd_variation_color = ssd_variation.colors[c];
                products['' + laptop.id + ssd_variation_color.color + ssd_variation.ssd + ''] = ssd_variation_color.id;
            }
        //}
        if(ssd_variation.colors.length==1) {
            //products['' + laptop.id + 'default' + ssd_variation.ssd + ''] = parseInt(ssd_variation.link.substr(1));
        }
    }
    html += '</ul>';
    html += '<div class="tab-content tab-content-' + laptop.id + ' custom-tab-content">';
    for (var v = 0; v < variations.length; v++) {
        var ssd_variation = variations[v];
        html += ' <div class="tab-pane ' + (v == 0 ? 'active' : '') + '" id="p' + laptop.id + '-v' + ssd_variation.ssd + '" role="tabpanel"><br />';
        html += '<div class="row custom-buttonWrapper-checkout">';
        html += '<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 custom-price-wrapper"><span class="text-left price-tag-label">' + ssd_variation.price + '</span><span class="lv-tag">лв.</span></div>';

        html += '<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 custom-buyNow-wrapper"><a href="' + ssd_variation.link + '">КУПИ СЕГА&nbsp;</a></div>';
        html += '</div>';
        html += '</div>';
    }
    html += "</div>";
    html += "</div>";
    html += '</div>';
    html += "</div>";
    html += "</div>";
    html += "<br /><br />";
    $("#products").append(html);
}

$(function () {

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        e.target // newly activated tab
        e.relatedTarget // previous active tab
        setVariant($(e.target));
    });

    function setVariant(el) {
        var ssd = el.data('ssd');
        var ram = el.data('ram');
        var hdd = el.data('hdd');
        var id = el.data('id');
        var t = el.data('type');
        var c = color[id];
        var v = el.data('value');
        value[id] = v;
        type[id] = t;
        
        if (typeof (c) != "undefined") {
            var k = id + '' + '' + c + '' + v;
        }
        else {
            var k = id + 'default' + v;
        }
        if (ssd != '') {
            $('.description-ssd-' + id).html(ssd);
        }
        if (ram != '') {
            $('.description-ram-' + id).html(ram);
        }
        if (hdd != '') {
            $('.description-hdd-' + id).html(hdd);
        }

        $('.tab-content-' + id + ' .tab-pane.active a').attr('href', "/products/" + products[k]);
    }
   
    $('a.variant').on('click', function (e) {
        setVariant($(this));
    });

    // product colors script
    $('.img-thumb-color').on('click', function (e) { 
        var colorId = $(this).data('colorid'); // Base color ID
        var id = $(this).data('id'); // Base product ID
        var c = $(this).data('color');
        color[id] = c;
        var k = id+''+''+c+''+value[id];
        var newProductId = products[k];
        if(typeof(newProductId)=='undefined') {
            newProductId = colorId;
        }
        $('.big-img-' + id).attr('src', $(this).attr('src'));
        $('.tab-content-' + id + ' .tab-pane.active a').attr('href', "/products/" + newProductId);
    });
});


